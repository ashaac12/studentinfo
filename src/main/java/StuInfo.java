import java.util.Arrays;
/**
 * @Author asha achhami
 * @Date 6/21/2018
 **/
public class StuInfo {
    private String[] StuInfo;

    /*public String[] getStuInfo() {
        return StuInfo;
    }*/

    public  void setStuInfo(String[] stuInfo) {
        this.StuInfo = stuInfo;
        this.printin();
    }
    public void printin(){
        /*sorting the name by ascending order each and every line by line*/
        Arrays.sort(this.StuInfo);
        System.out.println("ascending order :");
        Arrays.stream(this.StuInfo).forEach(System.out::println);
    }
}
