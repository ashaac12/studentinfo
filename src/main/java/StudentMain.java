import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.Scanner;
/**
 * @Author Asha Achhami
 * @Date 6/21/2018
 **/
public class StudentMain {
    public static void main(String[] args) {
        System.out.print("Enter the number of Students ");
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        String arrayst[] = new String[num];
        System.out.println("There are " + num + " students");
        for (int i = 0; i < arrayst.length; i++) {
            arrayst[i] = sc.next();
        }

        /*getting Spring bean */
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        StuInfo st = (StuInfo) context.getBean("StuInfo");

        st.setStuInfo(arrayst);
    }
}
